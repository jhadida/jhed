#!/bin/bash

if [ $# -gt 0 ]; then 
    PORT=$1
else
    PORT=3001
    echo "Usage: $0 <port>"
    echo "Setting default port: $PORT"
fi
python -m SimpleHTTPServer $PORT
