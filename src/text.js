
const $ = require( 'jquery' );
const Util = require( './util.js' );

// -------------------- ========== --------------------

/**
 * Find the beginning and end of each line in the input text.
 * This implementation includes empty lines, and supports CRLFs.
 *
 * Example:
 * >> str = 'Hello\nWor\rWorld\n\r\n';
 * >> lines = delineate( str )
 * [ { start: 0, end: 5, len: 5, nl: 1 },
 *   { start: 6, end: 15, len: 9, nl: 1 },
 *   { start: 16, end: 16, len: 0, nl: 2 } ]
 * >> lines.forEach( L => console.log(JSON.stringify( str.substring(L.start,L.end) )) );
 * "Hello"
 * "Wor\rWorld"
 * ""
 */
function delineate(txt) {

    // find all newlines
    var re = RegExp('\r?\n','g');
    var line = {};
    var match = null;
    var lines = [];
    var offset = 0;
    while ( match = re.exec(txt) ) {
        line = {
            'start': offset,
            'end': match.index,
            'len': match.index - offset,
            'nl': match[0].length
        };
        lines.push(line);
        offset = line.end + line.nl;
    }

    // if there is no newline at the end of file
    if ( (line.end + line.nl) < txt.length ) {
        lines.push({
            'start': offset,
            'end': txt.length,
            'len': txt.length - offset,
            'len': 0
        });
    }

    return lines;
}

/**
 * Given a set of lines and an absolute position, find the line corresponding to that position.
 */
function pos2line( lines, pos ) {
    var L = 0;
    while ( (L < lines.length) && (pos > lines[L].end) ) L++;
    return L;
}

/**
 * Define behaviour of tab key in textarea element.
 */
function setupTextarea( elm, tabstr, hotkeys ) {

    const tablen = tabstr.length;
    elm.keydown(function(e) {

        const elm = $(this);
        switch ( e.keyCode ) {
        case 9: // TAB

            const val = elm.val();
            const lines = delineate(val);
            const re = RegExp( '^' + tabstr );
            var cur = Util.getCursor(elm);

            // no selection: insert tab
            // single-line selection: delete selection and insert tab
            // multi-line selection: preserve lines, but prepend tab

            // multiline processing
            cur.sline = pos2line(lines, cur.start);
            cur.eline = pos2line(lines, cur.end);
            if ( cur.sel ) {

                // clamp to beginning and end of line
                cur.start = lines[cur.sline].start;
                cur.end = lines[cur.eline].end;

                // edit each line
                var edited = lines.slice( cur.sline, cur.eline+1 );
                if ( e.shiftKey ) {
                    edited = edited.map( (x) => val.slice(x.start,x.end).replace(re,'') ).join("\n");
                } else {
                    edited = edited.map( (x) => tabstr + val.slice(x.start,x.end) ).join("\n");
                }

                // new modification
                this.setSelectionRange( cur.start, cur.end );
                document.execCommand( 'insertText', false, edited );
                this.setSelectionRange( cur.start, cur.start+edited.length );

            } else {

                if ( e.shiftKey ) {

                    // clamp to beginning and end of line
                    cur.start = lines[cur.sline].start;
                    cur.end = lines[cur.eline].end;

                    edited = val.slice( cur.start, cur.end ).replace(re,'');
                    this.setSelectionRange( cur.start, cur.end );
                    document.execCommand( 'insertText', false, edited );
                    this.selectionStart = this.selectionEnd = cur.start;

                } else {

                    document.execCommand( 'insertText', false, tabstr );
                    this.selectionStart = this.selectionEnd = cur.start + tablen;

                }
            }

            // prevent the focus lose
            e.preventDefault();
            break;

        default: // try to match a hotkey
            var b = null;
            if ( e.ctrlKey && (b = hotkeys[e.keyCode]) ) {
                b.trigger('click');
                e.stopPropagation();
                e.preventDefault();
            }
        }
    });

    elm.keyup(function (e) {

        const elm = $(this);
        function refocus() {
            elm.blur();
            elm.focus();
        }

        switch ( e.keyCode ) {
            case 9: // TAB
                refocus();
                break;

            case 90: // Z
                if ( e.ctrlKey ) refocus();
                break;
        }
    });
}

// -------------------- ========== --------------------

module.exports = {
    'delineate': delineate,
    'pos2line': pos2line,
    'textarea': setupTextarea
};
