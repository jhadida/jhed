
const $ = require( 'jquery' );
const _ = require( 'lodash' );
const assert = require( 'assert' );

const Util = require( './util.js' );
const Text = require( './text.js' );

// -------------------- ========== --------------------

function cursorWrap( elm, wstr ) {
    var ta = elm.get(0);
    var wlen = wstr.length;
    var cur = Util.getCursor(elm);
    var edited = wstr + elm.val().slice(cur.start,cur.end) + wstr;

    elm.focus();
    document.execCommand( 'insertText', false, edited );
    ta.setSelectionRange( cur.start+wlen, cur.end+wlen );
}

function quote() {

}

function code() {

}

function list() {

}

function olist() {

}

// -------------------- ========== --------------------

module.exports = {
    'bold': (elm) => cursorWrap(elm,'**'),
    'emph': (elm) => cursorWrap(elm,'_'),
    'strike': (elm) => cursorWrap(elm,'~~')
};
