
const $ = require( 'jquery' );
const _ = require( 'lodash' );
const assert = require( 'assert' );

const Util = require('./util.js');
const Edit = require('./edit.js');
const Text = require('./text.js');

const MarkdownIt = require( 'markdown-it' );
const md_emoji = require( 'markdown-it-emoji' );
const md_highlight = require( 'markdown-it-highlightjs' );
const md_katex = require( 'markdown-it-katex' );
const md_mathjax = require( 'markdown-it-mathjax' );

// -------------------- ========== --------------------

/**
 * Convenient function to create edit buttons.
 */
function mkButton( contents, description, hotkey, callback ) {
    return $('<button>',{ 'class': 'ui button' }).html(contents)
        .data({ 'callback': callback, 'hotkey': hotkey, 'description': description });
}
function mkIconButton( name, description, hotkey, callback ) {
    return $('<button>',{ 'class': 'ui small compact basic grey icon button' })
        .append( $('<i>',{ 'class': name + ' icon' }) )
        .data({ 'callback': callback, 'hotkey': hotkey, 'description': description });
}

/**
 * Main function to build editor
 */
function buildEditor( id, cfg ) {

    // unique ID for this editor
    const uid = _.uniqueId();
    var mkid = (s) => 'jhed:' + s + '_' + uid;

    // default options
    cfg = _.merge({
        'tabstr': "    ",
        'menu-htm': [ 'Write', 'Preview' ],
        'menu-color': 'blue',
        'form-title': true,
        'form-title-ph': 'Title',
        'form-text-ph': 'Type your text here...',
        'edit-buttons': [
            mkIconButton('bold','Bold text','B',Edit.bold),
            mkIconButton('italic','Emphasize text','I',Edit.emph),
            mkIconButton('strikethrough','Strikethrough text','K',Edit.strike)
        ],
        'form-buttons': [],
        'md-use-katex': true,
        'md-opt': {
            'html': false,
            'xhtmlOutput': true,
            'breaks': true,
            'linkify': true
        },
        'md-opt-highlight': {},
        'md-opt-emoji': {},
        'md-opt-maths': {}
    }, cfg || {} );

    // configure renderer
    var md = new MarkdownIt( cfg['md-opt'] );
        md.use( md_highlight, cfg['md-opt-highlight'] );
        md.use( md_emoji, cfg['md-opt-emoji'] );
        md.use( md_katex, cfg['md-opt-maths'] );
        if ( !cfg['md-use-katex'] ) { // TODO: support MathJax too
            console.error('Sorry, but MathJax support is not implemented at this point.');
        }

    // UI components
    var cmpt = {
        'segment': Util.mkTag( mkid('seg'), 'ui attached fluid segment' ),
        'menu': Util.mkTag( mkid('menu'), 'ui secondary pointing menu' ),
        'edit': Util.mkTag( mkid('edit'), 'right menu' ),
        'form': Util.mkTag( mkid('form'), 'ui form' ),
        'preview': Util.mkTag( 'article', mkid('prev'), 'ui container markdown-body' ), // .markdown-body for styling
        'message': Util.mkTag( mkid('msg'), 'ui bottom attached message' ).css({
            'font-size': 'small', 'font-style': 'italic', 'color': '#aaa'
        })
    };

    // menu items (write and preview)
    var item_class = cfg['menu-color'] + ' item';
    var items = [
        Util.mkTag( mkid('lmenu-w'), item_class )
            .html(cfg['menu-htm'][0]).css('cursor','pointer').click(() => f_state('write')),
        Util.mkTag( mkid('lmenu-p'), item_class )
            .html(cfg['menu-htm'][1]).css('cursor','pointer').click(() => f_state('preview'))
    ];

    // insert edit buttons
    cmpt['edit'].append( cfg['edit-buttons'].map( function(b) {
        return $('<span>',{ 'class': 'item' })
            .css({'padding': '0.8em 0.1em'}).append(b);
    }));

    // form fields (title and text editor)
    var fields = {
        'title': $('<input>',{
            'type': 'text',
            'placeholder': cfg['form-title-ph']
        }),
        'text': $('<textarea>',{
            'id': mkid('text'),
            'placeholder': cfg['form-text-ph']
        })
    };

    // UI controllers
    var ctrl = {
        'cmpt': cmpt,
        'status': (msg) => cmpt['message'].html(msg),
        'title': () => fields['title'].val(),
        'text': () => fields['text'].val(),
        'render': f_render,
        'state': f_state
    };

    // append to form
    var wrapField = (x) => Util.mkTag('ui field').append(x);
    if ( cfg['form-title'] ) {
        cmpt['form'].append( wrapField(fields['title']), wrapField(fields['text']), cfg['form-buttons'] );
    } else {
        cmpt['form'].append( wrapField(fields['text']), cfg['form-buttons'] );
    }

    // process buttons
    var hotkeys = {};
    function processButton( but, arg ) {
        var hk = but.data('hotkey');
        var dc = but.data('description');

        if (hk) {
            hk = hk.toUpperCase();
            dc += ' (Ctrl+' + hk + ')';
            hotkeys[ hk.charCodeAt(0) ] = but; // FIRST CHAR ONLY
        }

        but.prop( 'title', dc ).click( function(e) {
            but.data('callback')( arg );
            e.preventDefault(); // dont lose focus
        });
    }
    cfg['form-buttons'].forEach( b => processButton(b,ctrl) ); // callback( controller )
    cfg['edit-buttons'].forEach( b => processButton(b,fields['text']) ); // callback( textarea )

    // setup editing behaviour in textarea
    Text.textarea( fields['text'], cfg['tabstr'], hotkeys );

    // build UI
    Util.getElement(id).append(
        cmpt['segment'].append(
            cmpt['menu'].append( items, cmpt['edit'] ),
            cmpt['form'],
            cmpt['preview']
        ),
        cmpt['message']
    );

    /**
     * Render text currently written into preview element, and return html.
     */
    function f_render() {
        var html = md.render( fields['text'].val() );
        cmpt['preview'].html( html );
        return html;
    }

    /**
     * State switching (write/preview).
     * Returns current state if called without input.
     */
    var m_state = null;
    function f_state(state) {
        switch ( state ) {
            case 'write':
                items[0].addClass('active');
                items[1].removeClass('active');
                cmpt['preview'].hide();
                cmpt['form'].show();
                break;
            case 'preview':
                items[0].removeClass('active');
                items[1].addClass('active');
                cmpt['form'].hide();
                cmpt['preview'].show();
                f_render();
                break;
            default:
                return m_state;
        }
        m_state = state;
    }

    // initialise UI
    f_state('write');
    return ctrl;
}

// -------------------- ========== --------------------

module.exports = {
    'edit': Edit,
    'build': buildEditor,
    'mkButton': mkButton,
    'mkIconButton': mkIconButton
};
