
const $ = require( 'jquery' );
const _ = require( 'lodash' );
const assert = require( 'assert' );

// -------------------- ========== --------------------

/**
 * Get jQuery object with any character in the ID (eg: 'foo:bar-baz').
 */
function getElement( id ) {
    var el = $( document.getElementById(id) || id );
    assert( el.length > 0, 'Element id "' + id + '" does not exist.' );
    return el;
}

/**
 * Return cursor state in textarea or input.
 */
function getCursor( elm ) {
    var ss = elm.prop( 'selectionStart' );
    var se = elm.prop( 'selectionEnd' );
    return {
        'start': ss,
        'end': se,
        'len': se - ss,
        'sel': se > ss
    };
}

/**
 * Shorthand to quickly create new elements:
 *  o 1 input: class
 *      Create div without id and specified class
 * 
 *  o 2 inputs: id, class
 *      Create div with specified id and class
 * 
 *  o 3 inputs: tag, id, class
 *      Create element with specified tag, id and class
 */
function mkTag(tg, id, cl) {

    if (!cl) { 
        cl = !id ? tg : id; 
        id = !id ? id : tg;
        tg = 'div';
    }

    return !id ?
        $('<'+tg+'>',{ 'class': cl }) :
        $('<'+tg+'>',{ 'id': id, 'class': cl }) ;
}

// -------------------- ========== --------------------

module.exports = {
    'getElement': getElement,
    'getCursor': getCursor,
    'mkTag': mkTag
};
