// 
const webpack = require('webpack');
const path = require('path');
const libraryName = 'jhed';

// plugins
var plugins = [];

// 
module.exports = {
    entry: path.resolve(__dirname,'src/main.js'),
    externals: {
        'jquery': 'jQuery',
        'lodash': '_',
        'katex': 'katex',
        'highlight.js': 'hljs'
    },
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: libraryName + '.js',
        library: libraryName
    },
    plugins: plugins
};