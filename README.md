
# jhed: A simple editor for Markdown and maths using KaTeX and SemanticUI

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Demo

```
git clone https://gitlab.com/jhadida/jhed
cd jhed
yarn install
yarn build
yarn demo
```

## Usage 

To do.

For now, look in `demo/index.html` for an example (specifically the line with `var editor = jhed.build ...`).

The various available options (to be passed as an object to the `build` method) can be found in `src/main.js`, specifically the line with `cfg = _.merge({ ...`.

The prefix `md` stands for Markdown, and the corresponding options relate to the package [MarkdownIt](https://www.npmjs.com/package/markdown-it).
The options with the prefix `form` and `menu` control various appearance details. 
The suffix `ph` stands for placeholder.

A few examples of edit buttons are also provided in `src/main.js` for bold / emph / strike; this should be clear enough to get you going with your own implementations.
An example of form button is shown in `demo.index.html`, in which case the third argument (here `'S'`) corresponds to a hotkey.

